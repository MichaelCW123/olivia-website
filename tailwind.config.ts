import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      screens: {
        tall: { raw: "(max-height: 800px)" },
      },
      fontFamily: {
        rubik: ["Rubik", "sans-serif"],
        flower: ["Indie Flower", "cursive"],
        kanit: ["Kanit", "sans-serif"],
      },
      animation: {
        typewriter: "typewriter 2s steps(11) forwards",
        shake: "shake 0.82s cubic-bezier(.36,.07,.19,.97) both",
        "loop-scroll": "loop-scroll 80s linear infinite",
        scrollLarge: "scroll 30s linear infinite",
        scrollPhone: "scroll 15s linear infinite",
      },
      keyframes: {
        typewriter: {
          to: {
            left: "100%",
          },
        },
        scroll: {
          "0%": { transform: "translateX(0)" },
          "100%": { transform: "translateX(-100%)" },
        },
      },
    },
  },
  plugins: [require("daisyui"), require("tailwindcss-text-border")],
};

export default config;
