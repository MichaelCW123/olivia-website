import WelcomePage from './welcome/WelcomePage'
import { Navbar } from './components/Navbar'

export default function Home() {
  return (
    <div className='flex flex-col w-screen h-screen bg-gradient-to-bl from-pink-400 to-rose-300'>
      <WelcomePage />
    </div>
  )
}
