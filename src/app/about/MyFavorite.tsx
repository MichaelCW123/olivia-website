'use client'
import { useEffect, useState } from 'react'
import ArtistCards from '../components/ArtistCards'
import ArtistSection from '../components/ArtistSection'
import ComposerSection from '../components/ComposerSection'

const MyFavorite: React.FC = () => {
  const [isMobile, setIsMobile] = useState(false)

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth < 768)
    }

    if (typeof window !== 'undefined') {
      // Set initial state
      handleResize()
      // Add event listener
      window.addEventListener('resize', handleResize)

      return () => {
        window.removeEventListener('resize', handleResize)
      }
    }
  }, [])

  return (
    <>
      {isMobile ? (
        <ArtistCards />
      ) : (
        <>
          <ComposerSection />
          <ArtistSection />
        </>
      )}
    </>
  )
}

export default MyFavorite
