import Image from "next/image";
import "animate.css";

const AboutMePage = () => {
  return (
    <div className="grow">
      <div
        className="h-[92vh] w-full flex flex-col justify-center items-center md:inline-block text-white"
        id="stagger-visualizer"
      >
        <div className="flex flex-col items-center h-full justify-center">
          <div className="flex flex-row justify-center w-full md:w-auto h-max">
            <div className="flex flex-col justify-center items-center w-3/4 md:w-max">
              <Image
                className=""
                src="/oliv.png"
                width={350}
                height={350}
                alt="Background blob"
              />
              <h1 className="text-center text-3xl font-bold font-serif pt-5">
                Olivia Agatha
              </h1>
            </div>
          </div>
          <div className="px-10 pt-5 mb-5 md:w-2/3">
            <h1 className="tall:text-lg text-xl font-bold md:text-3xl md:pb-10 text-center font-serif	">
              Olivia is a piano teacher who loves sharing music with her
              students. In her free time, she enjoys playing Valorant and GTA
              roleplay, balancing her love for music and gaming.
            </h1>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AboutMePage;
