import FooterComponent from "../components/Footer";
import { Navbar } from "../components/Navbar";
import AboutMePage from "./AboutMe";
import MyFavorite from "./MyFavorite";

export default function About() {
  return (
    <div className="h-full flex flex-col bg-gradient-to-bl from-pink-400 to-rose-300">
      <Navbar isNavbarOpen />
      <AboutMePage />
      <MyFavorite />
      <FooterComponent />
    </div>
  );
}
