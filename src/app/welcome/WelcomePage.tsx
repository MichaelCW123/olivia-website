'use client'

import 'animate.css'
import anime from 'animejs'
import { useEffect, useRef, useState } from 'react'
import { Navbar } from '../components/Navbar'
import Cookies from 'js-cookie'
import CarouselSlider from '../components/CarouselSlider'

const WelcomePage: React.FC = () => {
  const [cardVisible, setCardVisible] = useState(false)
  const [inputValue, setInputValue] = useState('')
  const [chance, setChance] = useState(3)
  const [error, setError] = useState(false)
  const buttonRef = useRef<HTMLButtonElement>(null)
  const [isNavbarOpen, setIsNavbarOpen] = useState(false)
  const [textVisible, setTextVisible] = useState(false)
  const [caroSliderVisible, setCaroSliderVisible] = useState(false)
  const carouselRef = useRef<HTMLDivElement>(null)

  const savedState = Cookies.get('isNavbarOpen')

  useEffect(() => {
    setIsNavbarOpen(savedState ? JSON.parse(savedState) : false)
  }, [])

  useEffect(() => {
    if (window.innerWidth < 768) {
      animateMobile()
    } else {
      setTimeout(() => {
        animateDesktop()
      }, 100)
    }
  }, [isNavbarOpen])

  useEffect(() => {
    if (caroSliderVisible) {
      anime({
        targets: carouselRef.current,
        opacity: [0, 1],
        duration: 2000,
        easing: 'easeOutExpo',
      })
    }
  }, [caroSliderVisible])

  useEffect(() => {
    if (cardVisible) {
      anime({
        targets: '.card',
        opacity: [0, 1],
        translateY: '30%',
        duration: 500,
        easing: 'easeOutExpo',
      })
    }
  }, [cardVisible])

  useEffect(() => {
    if (textVisible) {
      if (window.innerWidth < 768) {
        if (isNavbarOpen) {
          anime({
            targets: '#welcome-text',
            opacity: [0, 1],
            translateY: -200,
            duration: 500,
            easing: 'easeOutExpo',
          })
        } else {
          anime({
            targets: '#welcome-text',
            opacity: [0, 1],
            translateY: -200,
            duration: 500,
            easing: 'easeOutExpo',
          })
        }
      } else {
        anime({
          targets: '#welcome-text',
          opacity: [0, 1],
          translateY: '-80%',
          duration: 500,
          easing: 'easeOutExpo',
        })
      }
    }
  }, [textVisible])

  const handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === 'Enter') {
      buttonRef.current?.click()
    }
  }

  const handleSubmit = () => {
    const answer = inputValue.toLowerCase()
    if (answer !== 'valorant') {
      if (chance > 0) {
        setChance(chance - 1)
        setInputValue('')
        setError(true)
      }
    } else {
      setIsNavbarOpen(true)
      setChance(3)
      setError(false)
      setInputValue('')
      Cookies.set('isNavbarOpen', 'true')
      animateQuizCompleted()
    }
  }

  const animateDesktop = () => {
    const loader = anime.timeline({
      easing: 'easeOutExpo',
      duration: 750,
    })
    loader
      .add({
        targets: 'section div',
        width: '100%',
        backgroundColor: 'rgb(244, 114, 182)',
        delay: anime.stagger(100),
      })
      .add({
        targets: 'section div',
        width: '90%',
        backgroundColor: 'rgb(246,127,180)',
      })
      .add({
        targets: 'section div',
        width: '100%',
        backgroundColor: 'transparent',
        delay: anime.stagger(100),
        complete: () => {
          if (isNavbarOpen) {
            setTextVisible(true)
            setTimeout(() => setCaroSliderVisible(true), 1000)
          } else {
            setCardVisible(true)
            setTimeout(() => setTextVisible(true), 1000)
          }
        },
      })
  }

  const animateMobile = () => {
    anime({
      targets: '#welcome-text',
      duration: 1000,
      easing: 'easeOutElastic(1, .8)',
      complete: () => {
        console.log(isNavbarOpen)
        if (isNavbarOpen) {
          setTextVisible(true)
          setTimeout(() => setCaroSliderVisible(true), 1000)
        } else {
          setCardVisible(true)
          setTimeout(() => setTextVisible(true), 1000)
        }
      },
    })
  }

  const animateQuizCompleted = () => {
    anime({
      targets: '.card',
      opacity: [1, 0],
      duration: 1000, // 1 second
      easing: 'easeOutSine',
      complete: () => {
        setCardVisible(false)
        setCaroSliderVisible(true)
      },
    })
  }

  return (
    <>
      <Navbar isNavbarOpen={isNavbarOpen} />
      <div className='h-screen w-screen flex flex-row justify-center md:h-screen md:inline-block md:bg-white'>
        <div className='w-full h-full flex flex-col font-bold font-rubik text-white px-10 md:px-0 justify-center items-center'>
          <div className={`flex w-full absolute px-7 ${isNavbarOpen ? 'justify-start' : 'justify-center'}`}>
            {textVisible ? (
              <h1 className={`font-kanit text-white text-[5rem] md:text-[6rem] w-min md:w-max pb-10 md:px-20 leading-none ${isNavbarOpen ? 'text-start' : 'text-center'}`} id='welcome-text'>
                Olivia Agatha
              </h1>
            ) : null}
          </div>
          {!isNavbarOpen && cardVisible ? (
            <div className='card glass w-full md:w-1/2 xl:w-1/4 max-w-[400px] shadow-xl md:absolute text-white' id='question-card'>
              <div className='card-body'>
                <h2 className='card-title'>Love's Quiz</h2>
                <p>What is my favorite game?</p>
                <input type='text' placeholder='Type here' className='input input-bordered w-full text-black bg-white' value={inputValue} onChange={(e) => setInputValue(e.target.value)} onKeyDown={handleKeyPress} />
                <button className='btn bg-slate-900 text-white border-0' onClick={handleSubmit} ref={buttonRef}>
                  Unlock
                </button>
                {error ? <h2 className='text-end text-red-700'>Salah tut !!!</h2> : null}
              </div>
            </div>
          ) : caroSliderVisible ? (
            <CarouselSlider ref={carouselRef} />
          ) : null}

          <section className='hidden grid-cols-5 xl:grid-cols-10 w-full h-full md:grid'>
            <div className='bg-white h-full transition-colors duration-1000'></div>
            <div className='bg-white h-full transition-colors duration-1000'></div>
            <div className='bg-white h-full transition-colors duration-1000'></div>
            <div className='bg-white h-full transition-colors duration-1000'></div>
            <div className='bg-white h-full transition-colors duration-1000'></div>
            <div className='bg-white h-full transition-colors duration-1000 hidden xl:inline-block'></div>
            <div className='bg-white h-full transition-colors duration-1000 hidden xl:inline-block'></div>
            <div className='bg-white h-full transition-colors duration-1000 hidden xl:inline-block'></div>
            <div className='bg-white h-full transition-colors duration-1000 hidden xl:inline-block'></div>
            <div className='bg-white h-full transition-colors duration-1000 hidden xl:inline-block'></div>
          </section>
        </div>
      </div>
    </>
  )
}

export default WelcomePage
