'use client'

import DuckObject from '../model/DuckModel'

export default function Photocard() {
  return (
    <div className='h-full'>
      <div className='w-full flex flex-col justify-center items-center gap-7 lg:gap-10'>
        <div className='md:h-[600px] md:w-[800px]'>
          <DuckObject />
        </div>
        <h1 className='text-2xl font-bold md:text-4xl text-white'>~~ My Albums ~~</h1>
        <h3 className='w-3/4 lg:w-1/2 text-center text-white md:text-xl'>
          Welcome to "LoveGallery," a special album page full of pictures of me and my boyfriend. Here, you'll see our favorite moments together, captured in photos that show our love and fun times. From everyday moments to special events, each
          picture in LoveGallery shows how much we care for each other and the happiness we share. Enjoy looking through our memories!
        </h3>
      </div>
    </div>
  )
}
