import Carousel from "../components/Carousel";

export default function Photobox() {
  const slides = [
    "/albumPicture/album1.jpg",
    "/albumPicture/album2.jpg",
    "/albumPicture/album3.jpg",
    "/albumPicture/album4.jpg",
    "/albumPicture/album5.jpg",
  ];
  return (
    <Carousel autoSlide={false}>
      {slides.map((s: any) => (
        <img src={s} />
      ))}
    </Carousel>
  );
}
