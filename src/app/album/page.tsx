import FooterComponent from '../components/Footer'
import { Navbar } from '../components/Navbar'
import Timeline from '../components/Timeline'
import Moments from './moments'
import Photocard from './my-album'

export default function Album() {
  return (
    <div className='flex flex-col grow w-full h-full bg-gradient-to-bl from-pink-400 to-rose-300'>
      <Navbar isNavbarOpen={true} />
      <Photocard />
      <Moments />
      <Timeline />
      <FooterComponent />
    </div>
  )
}
