import Carousel from '../components/Carousel'

export default function Selfie() {
  const slides = ['/albumPicture/selfie2.jpg', '/albumPicture/selfie3.jpg', '/albumPicture/selfie4.jpg', '/albumPicture/selfie5.jpg', '/albumPicture/selfie6.jpg', '/albumPicture/selfie7.jpg', '/albumPicture/selfie8.jpg']
  return (
    <Carousel autoSlide={false}>
      {slides.map((s: any) => (
        <img src={s} />
      ))}
    </Carousel>
  )
}
