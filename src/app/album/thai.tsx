import Carousel from '../components/Carousel'

export default function Thai() {
  const slides = ['/albumPicture/thai1.jpg', '/albumPicture/thai2.jpg', '/albumPicture/thai3.jpg', '/albumPicture/thai4.jpg']
  return (
    <Carousel autoSlide={false}>
      {slides.map((s: any) => (
        <img src={s} />
      ))}
    </Carousel>
  )
}
