import Carousel from '../components/Carousel'

export default function Juicy() {
  const slides = ['/albumPicture/juicy1.jpg', '/albumPicture/juicy2.jpg', '/albumPicture/juicy3.jpg', '/albumPicture/juicy4.jpg', '/albumPicture/juicy5.jpg', '/albumPicture/juicy6.jpg']
  return (
    <Carousel autoSlide={false}>
      {slides.map((s: any) => (
        <img src={s} />
      ))}
    </Carousel>
  )
}
