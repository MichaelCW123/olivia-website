import Carousel from '../components/Carousel'

export default function Tretes() {
  const slides = ['/albumPicture/tretes1.jpg', '/albumPicture/tretes2.jpg', '/albumPicture/tretes3.jpg', '/albumPicture/tretes4.jpg']
  return (
    <Carousel autoSlide={false}>
      {slides.map((s: any) => (
        <img src={s} />
      ))}
    </Carousel>
  )
}
