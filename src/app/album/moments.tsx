"use client";
import { useState } from "react";
import Juicy from "./juicy";
import Thai from "./thai";
import Tretes from "./tretes";
import Selfie from "./selfie";
import Photobox from "./photobox";

export default function Moments() {
  const [photoboxOpen, setPhotoboxOpen] = useState(false);
  const [juicyOpen, setJuicyOpen] = useState(false);
  const [thaiOpen, setThaiOpen] = useState(false);
  const [tretesOpen, setTretesOpen] = useState(false);
  const [selfieOpen, setSelfieOpen] = useState(false);

  const handlePhotoboxOpen = () => {
    if (photoboxOpen == false) {
      setPhotoboxOpen(true);
    } else {
      setPhotoboxOpen(false);
    }
  };
  const handleJuicyOpen = () => {
    if (juicyOpen == false) {
      setJuicyOpen(true);
    } else {
      setJuicyOpen(false);
    }
  };
  const handleThaiOpen = () => {
    if (thaiOpen == false) {
      setThaiOpen(true);
    } else {
      setThaiOpen(false);
    }
  };
  const handleTretesOpen = () => {
    if (tretesOpen == false) {
      setTretesOpen(true);
    } else {
      setTretesOpen(false);
    }
  };
  const handleSelfieOpen = () => {
    if (selfieOpen == false) {
      setSelfieOpen(true);
    } else {
      setSelfieOpen(false);
    }
  };
  return (
    <div className="flex flex-col h-full items-center px-10 py-10">
      <h1 className="text-2xl font-bold md:text-4xl text-white pb-5">
        ~~ Moments ~~
      </h1>
      <div className="flex flex-col items-center w-full gap-5">
        <button
          className="btn btn-neutral w-full md:w-2/3 lg:w-1/3 text-lg rounded-full bg-[#dcdcde] border-0 text-[#bf229b] "
          onClick={handlePhotoboxOpen}
        >
          Photobox
        </button>
        {photoboxOpen && <Photobox />}
        <button
          className="btn btn-neutral w-full md:w-2/3 lg:w-1/3 text-lg rounded-full bg-[#dcdcde] border-0 text-[#bf229b]"
          onClick={handleJuicyOpen}
        >
          First Juicy Luicy Concert
        </button>
        {juicyOpen && <Juicy />}
        <button
          className="btn btn-neutral w-full md:w-2/3 lg:w-1/3 text-lg rounded-full bg-[#dcdcde] border-0 text-[#bf229b]"
          onClick={handleThaiOpen}
        >
          Thai Walk Walk
        </button>
        {thaiOpen && <Thai />}
        <button
          className="btn btn-neutral w-full md:w-2/3 lg:w-1/3 text-lg rounded-full bg-[#dcdcde] border-0 text-[#bf229b]"
          onClick={handleTretesOpen}
        >
          Tretes
        </button>
        {tretesOpen && <Tretes />}
        <button
          className="btn btn-neutral w-full md:w-2/3 lg:w-1/3 text-lg rounded-full bg-[#dcdcde] border-0 text-[#bf229b]"
          onClick={handleSelfieOpen}
        >
          Us On Selfie
        </button>
        {selfieOpen && <Selfie />}
      </div>
    </div>
  );
}
