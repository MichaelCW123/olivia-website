const Timeline: React.FC = () => {
  return (
    <div className="flex flex-col items-center justify-center px-5 text-white">
      <h1 className="text-2xl font-bold md:text-4xl text-white pb-5">
        ~~ Timeline ~~
      </h1>
      <ul className="timeline timeline-snap-icon max-md:timeline-compact timeline-vertical lg:w-1/2">
        <li>
          <div className="timeline-middle">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="#fff"
              className="h-5 w-5"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div className="timeline-start mb-10 text-end">
            <time className="font-mono italic border border-[#1D1D1D] rounded-full px-5 bg-[#1D1D1D] ">
              January 2024
            </time>
            <div className="text-lg font-black">
              First Time Valorant Together
            </div>
            I play valorant with my pookie together for the first time. I
            thought he can do better than me but it turns out he's really good
            at it.
          </div>
          <hr className="bg-black" />
        </li>
        <li>
          <hr className="bg-black" />
          <div className="timeline-middle">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              className="h-5 w-5"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div className="timeline-end mb-10">
            <time className="font-mono italic border border-[#1D1D1D] rounded-full px-5 bg-[#1D1D1D]">
              March 2024
            </time>
            <div className="text-lg font-black">
              Bruno Mars Concert in Bangkok
            </div>
            This is my first time going abroad alone. I have mixed feelings of
            fear and excitement. And what made me happy was that my pookie came
            to meet me at the airport.
          </div>
          <hr className="bg-black" />
        </li>
        <li>
          <hr className="bg-black" />
          <div className="timeline-middle">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              className="h-5 w-5"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div className="timeline-start mb-10 text-end">
            <time className="font-mono italic border border-[#1D1D1D] rounded-full px-5 bg-[#1D1D1D]">
              April 2024
            </time>
            <div className="text-lg font-black">
              Tretes with Cousins and Pookie
            </div>
            My cousin came to gather at my house. i invited my pookie to join
            this gathering. this is the first time my pookie met my cousin.
          </div>
          <hr className="bg-black" />
        </li>
        <li>
          <hr className="bg-black" />
          <div className="timeline-middle">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              className="h-5 w-5"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div className="timeline-end mb-10">
            <time className="font-mono italic border border-[#1D1D1D] rounded-full px-5 bg-[#1D1D1D]">
              May 2024
            </time>
            <div className="text-lg font-black">Juicy Luicy Concert</div>I
            really like juicy luicy. this is my first time watching juicy luicy
            concert. and most importantly, pookie came to watch with me.
          </div>
          <hr className="bg-black" />
        </li>
        <li>
          <hr className="bg-black" />
          <div className="timeline-middle">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
              fill="currentColor"
              className="h-5 w-5"
            >
              <path
                fillRule="evenodd"
                d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z"
                clipRule="evenodd"
              />
            </svg>
          </div>
          <div className="timeline-start mb-10 text-end">
            <time className="font-mono italic border border-[#1D1D1D] rounded-full px-5 bg-[#1D1D1D]">
              June 2024
            </time>
            <div className="text-lg font-black">Pookie's Thesis Defense</div>
            Pookie was initially worried about the thesis defense so I
            encouraged him by giving him a surprise.
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Timeline;
