"use client";
import { useRouter } from "next/navigation";

interface NavbarProps {
  isNavbarOpen: boolean;
}

export const Navbar: React.FC<NavbarProps> = ({ isNavbarOpen }) => {
  const router = useRouter();
  return isNavbarOpen ? (
    <div className="navbar bg-[transparent] md:static w-full md:px-10 text-white z-20">
      <div className="flex-1">
        <a className="text-xl font-bold pl-5 md:pl-0">Olivia.Agatha</a>
      </div>
      <div className="flex-none">
        <ul className="menu menu-horizontal text-lg gap-9">
          <li className="hidden md:inline">
            <a onClick={() => router.push("/")}>Home</a>
          </li>
          <li className="hidden md:inline">
            <a onClick={() => router.push("/album")}>Album</a>
          </li>
          <li className="hidden md:inline">
            <a onClick={() => router.push("/about")}>About Me</a>
          </li>
          <li>
            <details>
              <summary className="md:hidden">Menu</summary>
              <ul className="bg-[#dcdcde] rounded-t-none right-0 text-[#bf229b] font-semibold">
                <li>
                  <a onClick={() => router.push("/")}>Home</a>
                </li>
                <li>
                  <a onClick={() => router.push("/album")}>Album</a>
                </li>
                <li>
                  <a onClick={() => router.push("/about")}>About Me</a>
                </li>
              </ul>
            </details>
          </li>
        </ul>
      </div>
    </div>
  ) : null;
};
