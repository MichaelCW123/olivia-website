import Image from 'next/image'

const ComposerSection: React.FC = () => {
  return (
    <div className='flex flex-col w-full p-10 gap-5 bg-gradient-to-br from-pink-900 to-pink-700'>
      <h1 className='text-3xl md:text-[3rem] text-white self-center pb-10'>My Favorite Composer</h1>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box text-[#bf229b]'>
        <div className='hero-content flex-col xl:flex-row p-6'>
          <Image src='/favorite/beethovenbg.png' className='max-w-sm rounded-lg shadow-2xl' alt='Beethoven' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Beethoven</h1>
            <p className='py-6 text-lg'>
              Ludwig van Beethoven was a German composer and pianist born in 1770. He is famous for his beautiful music, which includes the Ninth Symphony and the Moonlight Sonata. Even though he lost his hearing later in life, he kept composing
              amazing pieces. Beethoven is one of the greatest musicians in history.
            </p>
          </div>
        </div>
      </div>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box self-end text-[#bf229b]'>
        <div className='hero-content flex-col xl:flex-row-reverse p-6'>
          <Image src='/favorite/chopinbg.png' className='max-w-sm rounded-lg shadow-2xl' alt='Chopin' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Chopin</h1>
            <p className='py-6 text-lg'>
              Frederic Chopin was a Polish composer and pianist born in 1810. He is known for his beautiful piano music, full of emotion and elegance. Some of his famous works include the Nocturnes and Etudes. Chopin's music is beloved for its
              delicate melodies and expressive depth, making him one of the greatest pianists and composers in history.
            </p>
          </div>
        </div>
      </div>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box text-[#bf229b]'>
        <div className='hero-content flex-col xl:flex-row p-6'>
          <Image src='/favorite/lisztbg.png' className='max-w-sm rounded-lg shadow-2xl' alt='Liszt' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Liszt</h1>
            <p className='py-6 text-lg'>
              Franz Liszt was a Hungarian composer and pianist born in 1811. He is famous for his incredible piano skills and exciting compositions. Some of his well-known works include the Hungarian Rhapsodies and the Transcendental Etudes. Liszt
              was a musical genius and a true showman, making him one of the greatest pianists and composers in history.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ComposerSection
