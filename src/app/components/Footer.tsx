"use client";
import Image from "next/image";

const FooterComponent = () => {
  const openDiscord = () => {
    location.href = "https://discord.gg/he3XWW8y";
  };
  const openInstagram = () => {
    location.href = "https://www.instagram.com/oliviaagathar/";
  };
  return (
    <footer className="flex flex-col px-10 md:px-20 py-5 bg-gradient-to-bl from-pink-400 to-rose-300 w-full text-black">
      <h1 className="font-bold text-xl xl:text-2xl text-white">
        Connect With Me:{" "}
      </h1>
      <div className="flex flex-row md: gap-3">
        <Image
          className=""
          src="/footer/discord.png"
          width={50}
          height={50}
          alt="discord"
          onClick={openDiscord}
        />
        <Image
          className=""
          src="/footer/ig.png"
          width={50}
          height={50}
          alt="instagram"
          onClick={openInstagram}
        />
      </div>
    </footer>
  );
};

export default FooterComponent;
