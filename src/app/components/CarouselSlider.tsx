import Image from "next/image";
import React, { forwardRef } from "react";

const CarouselSlider = forwardRef<HTMLDivElement>((props, ref) => {
  const imageUrls = [
    "/albumPicture/album1.jpg",
    "/albumPicture/tretes3.jpg",
    "/albumPicture/selfie3.jpg",
    "/albumPicture/juicy1.jpg",
    "/albumPicture/thai1.jpg",
    "/albumPicture/album3.jpg",
    "/albumPicture/selfie1.jpg",
  ];

  const doubledImageUrls = [...imageUrls, ...imageUrls]; // Duplicate for smooth loop

  return (
    <div
      ref={ref}
      className="carousel-container overflow-hidden h-1/2 absolute bottom-10 w-full"
    >
      <div className="flex w-[150%] lg:w-3/4 animate-scrollPhone lg:animate-scrollLarge space-x-4">
        {doubledImageUrls.map((url, index) => (
          <div key={index} className="carousel-item flex-shrink-0 w-1/4 p-2">
            <Image
              src={url}
              className="w-full h-full object-cover rounded-xl"
              alt=""
              width={250}
              height={250}
            />
          </div>
        ))}
      </div>
    </div>
  );
});

export default CarouselSlider;
