import Image from "next/image";

const ArtistCards = () => {
  return (
    <>
      <div className="h-full">
        <div className="w-full flex flex-col justify-center items-center gap-7 lg:gap-10">
          {/* Composer Card */}
          <h1 className="text-3xl font-bold md:text-4xl text-white">
            My Favorite Composer
          </h1>
          <div className="lg:w-full flex flex-col flex-wrap md:flex-row items-center justify-center gap-7 lg:gap-20 shrink  px-20 rounded-3xl text-[#bf229b] font-semibold">
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10">
                <Image
                  className=""
                  src="/favorite/beethovenbg.png"
                  width={300}
                  height={400}
                  alt="Beethoven"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Beethoven</h2>
                <p>German composer, pianist, Classical-Romantic bridge.</p>
              </div>
            </div>
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10">
                <Image
                  className=""
                  src="/favorite/chopinbg.png"
                  width={300}
                  height={400}
                  alt="Chopin"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Chopin</h2>
                <p>Polish composer, virtuoso pianist, Romantic era.</p>
              </div>
            </div>
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10">
                <Image
                  className=""
                  src="/favorite/lisztbg.png"
                  width={300}
                  height={400}
                  alt="Mozart"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Liszt</h2>
                <p>Hungarian composer, virtuoso, Romantic era.</p>
              </div>
            </div>
          </div>
          {/* Artist Card */}
          <h1 className="text-3xl font-bold md:text-4xl text-white mt-10">
            My Favorite Musician
          </h1>
          <div className="lg:w-full flex flex-col flex-wrap md:flex-row items-center justify-center gap-7 lg:gap-20 shrink pb-10 px-20 rounded-3xl text-[#bf229b]">
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10 h-40">
                <Image
                  className=""
                  src="/favorite/seventeen.jpg"
                  width={300}
                  height={400}
                  alt="Seventeen"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Seventeen</h2>
                <ul className="w-40 text-start list-disc">
                  <li>Super</li>
                  <li>Home</li>
                  <li>Darl+ing</li>
                </ul>
              </div>
            </div>
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10 h-40">
                <Image
                  className=""
                  src="/favorite/juicybg.png"
                  width={300}
                  height={400}
                  alt="Juicy Luicy"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Juicy Luicy</h2>
                <ul className="w-40 text-start list-disc">
                  <li>Asing</li>
                  <li>Tampar</li>
                  <li>Terlalu Tinggi</li>
                </ul>
              </div>
            </div>
            <div className="card md:w-56 md:h-1/5 2xl:w-72 2xl:h-1/3 glass shadow-xl bg-[#dcdcde]">
              <figure className="px-10 pt-10 h-40">
                <Image
                  className=""
                  src="/favorite/coldiacbg.png"
                  width={300}
                  height={400}
                  alt="Coldiac"
                />
              </figure>
              <div className="card-body items-center text-center">
                <h2 className="card-title">Coldiac</h2>
                <ul className="w-40 text-start list-disc">
                  <li>I Don't Mind</li>
                  <li>Vow</li>
                  <li>Heart's Desire</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ArtistCards;
