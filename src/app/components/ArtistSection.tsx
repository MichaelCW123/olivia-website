import Image from 'next/image'

const ArtistSection: React.FC = () => {
  return (
    <div className='flex flex-col w-full p-10 gap-5'>
      <h1 className='text-3xl md:text-[3rem] text-white self-center py-10'>My Favorite Musician</h1>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box self-end text-[#bf229b] min-h-[348px]'>
        <div className='hero-content flex-col xl:flex-row-reverse p-6'>
          <Image src='/favorite/seventeen.jpg' className='max-w-sm rounded-lg shadow-2xl' alt='Seventeen' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Seventeen</h1>
            <p className='py-6 text-lg'>
              Seventeen is a popular K-pop boy band from South Korea, formed in 2015. The group has 13 members divided into three sub-units: hip-hop, vocal, and performance, showcasing their diverse talents. Known for their energetic performances and
              self-produced music, Seventeen has gained a large international fanbase and released many hit songs, including "Pretty U" and "Don't Wanna Cry."
            </p>
          </div>
        </div>
      </div>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box self-start text-[#bf229b] min-h-[348px]'>
        <div className='hero-content flex-col xl:flex-row p-6'>
          <Image src='/favorite/juicybg.png' className='max-w-sm rounded-lg shadow-2xl' alt='Juicy Luicy' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Juicy Luicy</h1>
            <p className='py-6 text-lg'>
              Juicy Luicy is an Indonesian music band known for their catchy tunes and heartfelt lyrics. Formed in 2016, the band blends pop, rock, and indie styles to create a unique sound. They gained popularity with hit songs like "Lantas" and
              "Tanpa Tergesa." Juicy Luicy's relatable music and engaging performances have earned them a loyal following in Indonesia and beyond.
            </p>
          </div>
        </div>
      </div>
      <div className='hero bg-[#dcdcde] w-3/5 rounded-box self-end text-[#bf229b] min-h-[348px]'>
        <div className='hero-content flex-col xl:flex-row-reverse p-6'>
          <Image src='/favorite/coldiacbg.png' className='max-w-sm rounded-lg shadow-2xl' alt='Coldiac' width={300} height={400} />
          <div>
            <h1 className='text-5xl font-bold'>Coldiac</h1>
            <p className='py-6 text-lg'>
              Coldiac is an Indonesian indie pop band known for their smooth melodies and soulful vocals. Formed in 2015, the band combines elements of pop, funk, and R&B to create a unique sound. They gained popularity with songs like "Vow" and
              "Beautiful Day" Coldiac's fresh and modern music has earned them a dedicated fanbase in the Indonesian indie music scene.
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
export default ArtistSection
